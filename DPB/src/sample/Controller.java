package sample;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.*;

public class Controller implements Initializable {
    @FXML
    private TextField backupTextField, restoreTextField;
    @FXML
    private TableView backupTableView, restoreTableView;
    @FXML
    private TableColumn backupPackageColumn;
    @FXML
    private TableColumn restorePackageColumn;
    @FXML
    private ProgressBar backupProgressBar;
    @FXML
    private ProgressBar restoreProgressBar;
    @FXML
    private Label backupPackageLabel;
    @FXML
    private Label restorePackageLabel;

    private ObservableList<Package> packagesList;
    private List<Package> packages = new ArrayList<Package>();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        String userName = System.getProperty("user.name");
        if (!userName.equals("root")){
            showErrorAlert("You must start this program as root!");
            System.exit(1);
        }
        fillTable(backupTableView, null);
    }

    public void chooseBackupDirectory() {
        String dir = openDirectoryChooser("Choose backup directory", backupTextField);
        if (dir != null ) {
            backupTextField.setText(dir);
        }
    }

    public void chooseRestoreDirectory() {
        String dir = openDirectoryChooser("Choose restore directory", restoreTextField);
        if (dir != null ) {
            restoreTextField.setText(dir);
        }
        fillTable(restoreTableView, restoreTextField.getText());
    }

    public void backup() {
        String backupDirectoryName = backupTextField.getText();
        if (backupDirectoryName.isEmpty())
        {
            showErrorAlert("Directory not selected!");
            return;
        }
        if (!isValidDirectory(backupDirectoryName))
        {
            showErrorAlert("Wrong path for backup directory!");
            return;
        }
        for (int i = 0; i < packages.size(); i++) {
            try {
                String pkgName = packages.get(i).getName();
//                backupProgressBar.setProgress((1.0*i)/packages.size());
//                backupPackageLabel.setText(pkgName);
                System.out.println(pkgName);
                System.out.println((1.0*i)/packages.size());
                ProcessBuilder pb = new ProcessBuilder("dpkg-repack", pkgName);
                pb.directory(new File(backupDirectoryName));
                Process process = pb.start();
                if (process.waitFor() != 0){
                    break;
                }
            }
            catch (IOException e){
                System.out.println(e);
                break;
            }
            catch (Exception e){
                System.out.println(e);
                break;
            }
        }
//        backupProgressBar.setProgress(1.0);
    }

    public void restore(){
        String restoreDirectoryName = restoreTextField.getText();
        if (restoreDirectoryName.isEmpty())
        {
            showErrorAlert("Directory not selected!");
            return;
        }
        if (!isValidDirectory(restoreDirectoryName))
        {
            showErrorAlert("Wrong path for backup directory!");
            return;
        }
        for (int i = 0; i < packages.size(); i++) {
            try {
                String pkgName = packages.get(i).getName();
//                backupProgressBar.setProgress((1.0*i)/packages.size());
//                backupPackageLabel.setText(pkgName);
                System.out.println(pkgName);
                System.out.println((1.0*i)/packages.size());
                ProcessBuilder pb = new ProcessBuilder("gdebi-gtk", "-n", pkgName);
                pb.directory(new File(restoreDirectoryName));
                Process process = pb.start();
                if (process.waitFor() != 0){
                    break;
                }
            }
            catch (IOException e){
                System.out.println(e);
                break;
            }
            catch (Exception e){
                System.out.println(e);
                break;
            }
        }
    }


    private void showErrorAlert(String text){
        Alert alert = new Alert(Alert.AlertType.ERROR, text);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            return;
        }
    }

    private boolean isValidDirectory(String dirpath){
        return new File(dirpath).exists();
    }

    private String openDirectoryChooser(String title, TextField textField) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        if (textField.getText().isEmpty()){
            directoryChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        }
        else {
            directoryChooser.setInitialDirectory(new File(textField.getText()));
        }

        directoryChooser.setTitle(title);
        File directory = directoryChooser.showDialog(null);
        if (directory != null) {
            return directory.getAbsolutePath();
        }
        else {
            return null;
        }
    }

     private void fillTable(TableView table, String dirname){
         if (dirname == null)
             fillBackupPackages();
         else
             fillRestorePackages(dirname);
         packagesList = FXCollections.observableList(packages);

         table.setItems(this.packagesList);
    }

    private void fillBackupPackages(){
        packages.clear();
        try {
            Process dpkg = new ProcessBuilder("dpkg", "--get-selections").start();
            InputStream is = dpkg.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;

            while ((line = br.readLine()) != null) {
                if (line.contains("deinstall"))
                    continue;
                else {
                    int i = line.indexOf("\t");
                    if (i != -1) {
                        String pkgName = line.substring(0, i);
                        Package pkg = new Package(pkgName);
                        packages.add(pkg);
                    }
                }
            }
        }
        catch (IOException e){
            System.out.println("Error calling dpkg!");
        }
    }

    private void fillRestorePackages(String dirname){
        packages.clear();
        File dir = new File(dirname);
        File [] files = dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".deb");
            }
        });
        for (File file : files) {
            Package pkg = new Package(file.getName());
            packages.add(pkg);
        }
    }
}
