package sample;


import javafx.beans.property.SimpleStringProperty;

public class Package {
    private final SimpleStringProperty name;

    public Package(String name){
        this.name = new SimpleStringProperty(name);
    }

    public String getName(){
        return name.get();
    }
}
